import { defineConfig, devices } from '@playwright/test';
import dotenv from 'dotenv';

dotenv.config();

export default defineConfig({
  testMatch: '*.spec.ts',
  fullyParallel: true,
  forbidOnly: true,
  workers: 2,
  reporter: [
    ['html', { open: 'never' }],
  ],

  use: {
    testIdAttribute: 'data-test',
    trace: 'retain-on-failure',
    ignoreHTTPSErrors: true,
    screenshot: 'only-on-failure',
  },

  projects: [
    {
      name: 'chromium',
      use: { ...devices['Desktop Chrome'] },
    },
  ],
});
