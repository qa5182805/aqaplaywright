# AQAPLAYWRIGHT

![test_passed](/uploads/387af372a9fcdebf4f351da370e6e87a/test_passed.png)

## Getting started
Create (design and implement) an automated API test

Create (design and implement) an automated UI test
# Tools to handle the tests
- Playwright v1.41.2
- Node JS 20.11.0

# Executing tests
## Local
- Clone repo
- Checkout to code dir
- Install  node modules, run ```npm install```
- On the terminal, run API test with ```npm run pw:run:api```
- On the terminal, run UI test with ```npm run pw:run:ui```
- On the terminal, run both API and UI tests with ```npm run pw:run:both```
- On the terminal, run UI test in a headed mode with ```npm run pw:run:ui:headed```
- On the terminal, view test report by running ```npm run pw:show:report```

## CI
- Tests run on Gitlab Pipelines on every push
- To trigger manually, navigate to https://gitlab.com/qa5182805/aqaplaywright/-/pipelines/new and click the Run Pipeline button
