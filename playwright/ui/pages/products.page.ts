import { Page, Locator } from '@playwright/test';

export class Products {
  readonly page: Page;
  readonly productSortContainer: Locator;
  readonly productsSortOrder: Locator;
  readonly productsName: Locator;

  constructor(page: Page) {
    this.page = page;
    this.productSortContainer = this.page.getByTestId('product_sort_container');
    this.productsSortOrder = this.page.locator('.active_option');
    this.productsName = this.page.locator('.inventory_item_name');
  }

  async changeProductsSortOrder(option: string): Promise<void> {
    await this.productSortContainer.selectOption(option);
  }
}