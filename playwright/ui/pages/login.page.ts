import { Page, Locator } from '@playwright/test';

export class Login {
  readonly page: Page;
  readonly userName: Locator;
  readonly password: Locator;
  readonly loginButton: Locator;

  constructor(page: Page) {
    this.page = page;
    this.userName = this.page.getByTestId('username');
    this.password = this.page.getByTestId('password');
    this.loginButton = this.page.getByTestId('login-button');
  }

  async goto(): Promise<void> {
    await this.page.goto(process.env.UI_BASE_URL!);
  }

  async login(username: string, password: string): Promise<void> {
    await this.goto();
    await this.userName.fill(username);
    await this.password.fill(password);
    await this.loginButton.click();
  }
}