import { test, expect } from '@playwright/test';
import { Login, Products } from '../pages';
import { isProductsSortedAscending, isProductsSortedDescending } from '../helpers/is_products_sorted';

test.describe('UI test - https://www.saucedemo.com/', () => {
  let productsPage: Products;
  test.beforeEach(async ({ page }) => {
    const loginPage: Login = new Login(page);
    await loginPage.login(process.env.USERNAME!, process.env.PASSWORD!);
    productsPage = new Products(page);
  });

  test('Login, retrieve products and verify products are sorted correctly', async ({ page }) => {
    // Verify that the products are sorted by Name ( A -> Z ).
    await expect(productsPage.productSortContainer).toBeVisible();
    await expect(productsPage.productsSortOrder).toContainText('Name (A to Z)');
    await expect(productsPage.productsName.first()).toContainText('Sauce Labs Backpack');
    expect(isProductsSortedAscending(await productsPage.productsName.allInnerTexts())).toEqual(true);
    
    await productsPage.changeProductsSortOrder('za');

    // Verify that the products are sorted by Name ( Z -> A ).
    await expect(productsPage.productsSortOrder).toContainText('Name (Z to A)');
    await expect(productsPage.productsName.first()).toContainText('Test.allTheThings() T-Shirt (Red)');
    expect(isProductsSortedDescending(await productsPage.productsName.allInnerTexts())).toEqual(true);
  })
})
