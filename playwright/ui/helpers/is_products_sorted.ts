export const isProductsSortedAscending = (products: string[])  => {
  for (let index = 1; index < products.length; index++) {
    if (products[index] < products[index - 1]) return false;
  }
  return true;
}

export const isProductsSortedDescending = (products: string[])  => {
  for (let index = 1; index < products.length; index++) {
    if (products[index] > products[index - 1]) return false;
  }
  return true;
}
