import { test, expect, APIResponse } from '@playwright/test';
import { CountEntriesObject, Entries } from '../models/types';

test.describe('API test - https://api.publicapis.org/entries', () => {
  let objectsWithCategoryAuthenticationAndAuthorization: Array<Entries>;

  test.afterAll(async () => {
    console.log('Printing found objects with "Category: Authentication & Authorization"');
    console.log(objectsWithCategoryAuthenticationAndAuthorization);
  });

  test('Get and verify the number of objects with "Category: Authentication & Authorization"', async ({ request }) => {
    const response: APIResponse = await request.get(process.env.API_BASE_URL!);
    expect(response.ok()).toBeTruthy();
    const responseBody: CountEntriesObject = await response.json();
    expect(responseBody).toHaveProperty('count');
    expect(responseBody).toHaveProperty('entries');
    objectsWithCategoryAuthenticationAndAuthorization = responseBody.entries.filter((entry: { Category: string; }) => entry.Category === 'Authentication & Authorization');
    // Verify the number of objects with "Category: Authentication & Authorization
    expect(objectsWithCategoryAuthenticationAndAuthorization.length).toEqual(7);
    expect(objectsWithCategoryAuthenticationAndAuthorization[0].Category).toEqual('Authentication & Authorization');
  });
})
