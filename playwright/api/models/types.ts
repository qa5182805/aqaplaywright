export type Entries = {
  API: string,
  Description: string,
  Auth: string,
  HTTPS: boolean,
  Cors: string,
  Link: string,
  Category: string
}

export type CountEntriesObject = {
  count: number,
  entries: [Entries]
}
